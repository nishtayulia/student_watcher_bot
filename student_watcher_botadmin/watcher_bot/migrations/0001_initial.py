# Generated by Django 4.1.3 on 2022-11-24 20:40

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('external_id', models.PositiveIntegerField(verbose_name='ID користувача в телеграм')),
                ('name', models.TextField(verbose_name="Ім'я користувача")),
            ],
            options={
                'verbose_name': 'Профіль',
            },
        ),
    ]
