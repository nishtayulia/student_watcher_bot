from django.apps import AppConfig


class WatcherBotConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'watcher_bot'
